package com.example.mario.ejercicio6.testrealm;

import android.test.AndroidTestCase;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.model.SerieDAO;

import org.junit.Test;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

import static junit.framework.Assert.assertNotNull;

public class RealmTest extends AndroidTestCase{
    @Test
    public void testCanConnectRealm(){
        Realm realm;
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();
        assertNotNull(realm);
    }
    public void testCanSaveAnObject(){
        Realm realm;
        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Serie serie = new Serie ("","image", "prueba", "prueba","2000");
        SerieDAO serieDAO = new SerieDAO();
        serieDAO.setSerieDAO(serie);
        realm.copyToRealmOrUpdate(serieDAO);
        realm.commitTransaction();
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).findAll();
        RealmObject object = r.get(0);
        SerieDAO newserieDAO = (SerieDAO) object;
        assertEquals(newserieDAO.getCode(),serie.getCode());
    }
}