package com.example.mario.ejercicio6.UI;

import java.util.HashMap;
import java.util.Map;

public class Serie {

    private String code;
    private String image;
    private String title;
    private String description;
    private String year;
    private boolean fav = false;
    private boolean mg = false;
    private long createdAt;
    private long updatedAt;
    private boolean active = true;
    private long meGusta_number;

    public Serie(){
        this("","","","","", false, false , 0 , 0, 0);
    }

    public Serie(String code, String image, String title, String description, String year, boolean fav, boolean mg, long createdAt, long updatedAt, long meGusta_number) {
        this.code = code;
        this.image = image;
        this.title = title;
        this.description = description;
        this.year = year;
        this.fav = fav;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.mg = mg;
        this.meGusta_number = meGusta_number;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", code);
        result.put("image", image);
        result.put("title", title);
        result.put("description", description);
        result.put("year", year);
        result.put("createdAt", createdAt);
        result.put("updatedAt", updatedAt);
        result.put("meGusta_number", meGusta_number);
        return result;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public void setMg(boolean mg) {
        this.mg = mg;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setMeGusta_number(long meGusta_number) {
        this.meGusta_number = meGusta_number;
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getYear() {
        return year;
    }

    public boolean getFav() {
        return fav;
    }

    public boolean getMg() {
        return mg;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public boolean getActive() {
        return active;
    }

    public long getMeGusta_number() {
        return meGusta_number;
    }
}

