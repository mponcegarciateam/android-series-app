package com.example.mario.ejercicio6.UI;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mario.ejercicio6.R;
import com.example.mario.ejercicio6.UI.activities.MainActivity;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.SerieViewHolder>{

    private List<Serie> series;
    private ClickListener clickListener;
    private Context context;

    public SeriesAdapter(ArrayList listSeries, Context context, ClickListener clickListener) {
        series = listSeries;
        this.clickListener = clickListener;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(final SerieViewHolder viewHolder, final int i) {
        final Serie item = series.get(i);
        viewHolder.tittle.setText(item.getTitle());
        Picasso.with(context).load(item.getImage()).into(viewHolder.image);
        viewHolder.year.setText(item.getYear());
        viewHolder.itemView.setTag(item);
        viewHolder.tvcontmg.setText(String.valueOf(item.getMeGusta_number()));
        if (item.getFav())
            viewHolder.ibfav.setImageResource(R.drawable.ic_star_yellow_32dp);
        else
            viewHolder.ibfav.setImageResource(R.drawable.ic_star_black_32dp);
        if (item.getMg())
            viewHolder.ibmg.setImageResource(R.drawable.ic_thumb_up_blue_32dp);
        else
            viewHolder.ibmg.setImageResource(R.drawable.ic_thumb_up_black_32dp);
        viewHolder.ibfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getFav()){
                    item.setFav(false);
                    viewHolder.ibfav.setImageResource(R.drawable.ic_star_black_32dp);
                }
                else{
                    item.setFav(true);
                    viewHolder.ibfav.setImageResource(R.drawable.ic_star_yellow_32dp);
                }
                clickListener.onFavClicked(item);
            }
        });

        viewHolder.ibmg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getMg()){
                    item.setMg(false);
                }
                else{
                    item.setMg(true);
                }
                clickListener.onMgClicked(item, i, viewHolder.ibmg, viewHolder.tvcontmg);
            }
        });
    }


    public static class SerieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ClickListener listener;
        ImageView image;
        TextView tittle;
        TextView year;
        ImageButton ibmg;
        TextView tvcontmg;
        ImageButton ibfav;


        public SerieViewHolder(View v, ClickListener listener) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            tittle = (TextView) v.findViewById(R.id.title);
            year = (TextView) v.findViewById(R.id.year);
            ibmg = (ImageButton) v.findViewById(R.id.ibmg);
            tvcontmg = (TextView) v.findViewById(R.id.tvcontmg);
            ibfav = (ImageButton) v.findViewById(R.id.ibfav);
            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Serie item = (Serie) v.getTag();
            listener.onItemClicked(item, getAdapterPosition());

        }
    }
        @Override
        public int getItemCount() {
            return series.size();
        }

        @Override
        public SerieViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_series, viewGroup, false);
            return new SerieViewHolder(v, clickListener);
        }

        public interface ClickListener {
            void onItemClicked(Serie item, int position);
            void onFavClicked(Serie item);
            void onMgClicked(Serie item, int position, ImageButton ibmg, TextView mgcount);
    }

}