package com.example.mario.ejercicio6.UI.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.mario.ejercicio6.R;
import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.model.MyFirebase;
import com.example.mario.ejercicio6.presenter.UploadImagePresenter;
import com.example.mario.ejercicio6.presenter.UploadSeriePresenter;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class AddSeriesActivity extends AppCompatActivity implements UploadImagePresenter.View, UploadSeriePresenter.View {
    private EditText ettitle, etdescription, etyear;
    private Button btuptdate;
    private ImageButton ibimage;
    private static final int SELECT_PICTURE = 1;
    private static int TAKE_PICTURE = 2;
    private ImageView ivseries;
    private UploadImagePresenter uploadImagePresenter;
    private UploadSeriePresenter uploadSeriePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_series);
        loadItems();
        ivseries.setVisibility(View.GONE);
        btuptdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadUploadImagePresenter();
                uploadImagePresenter.ReceiveData(ivseries, ettitle.getText().toString());
            }
        });

        ibimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerForContextMenu(v);
                openContextMenu(v);
            }
        });
    }

    public void loadItems(){
        ettitle = (EditText) findViewById(R.id.etTittle);
        etdescription = (EditText) findViewById(R.id.etDescription);
        etyear = (EditText) findViewById(R.id.etYear);
        btuptdate = (Button) findViewById(R.id.btUpdate);
        ibimage = (ImageButton) findViewById(R.id.ibimage);
        ivseries = (ImageView) findViewById(R.id.imageView);
    }
    public void abrirGaleria(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Seleccione una imagen"),
                SELECT_PICTURE);
    }
    public void abrirCamara(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            takePictureIntent.putExtra("crop", "true");
            takePictureIntent.putExtra("aspectX", 1);
            takePictureIntent.putExtra("aspectY", 1);
            takePictureIntent.putExtra("outputX", 150);
            takePictureIntent.putExtra("outputY", 150);
            startActivityForResult(takePictureIntent, TAKE_PICTURE);
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 1) {
                Uri uimage = data.getData();
                InputStream is;
                try {
                    is = getContentResolver().openInputStream(uimage);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    Bitmap bitmap = BitmapFactory.decodeStream(bis);
                    ivseries.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {}
            }
            if (requestCode == 2) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = extras.getParcelable("data");
                    ivseries.setImageBitmap(imageBitmap);
                }
            }
            ivseries.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.photo, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.gallery_photo:
                abrirGaleria();
                return true;
            case R.id.take_photo:
                abrirCamara();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void LoadUploadImagePresenter(){
        uploadImagePresenter = new UploadImagePresenter();
        uploadImagePresenter.setView(this);
    }

    public void LoadUploadSeriePresenter(){
        uploadSeriePresenter = new UploadSeriePresenter();
        uploadSeriePresenter.setView(this);
    }

    public Serie buildSerie(String url){
        Serie serie = new Serie();
        serie.setTitle(ettitle.getText().toString());
        serie.setDescription(etdescription.getText().toString());
        serie.setYear(etyear.getText().toString());
        serie.setCode(MyFirebase.getNewId());
        serie.setImage(url);
        serie.setCreatedAt(System.currentTimeMillis());
        serie.setUpdatedAt(System.currentTimeMillis());
        serie.setMeGusta_number(0);
        return serie;
    }

    @Override
    public void onImageUpload(String url) {
        Serie serie = buildSerie(url);
        LoadUploadSeriePresenter();
        uploadSeriePresenter.ReceiveData(serie);
    }

    @Override
    public void onImageNotUpload(String error) {
        Toast toast = Toast.makeText(this,error,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onSerieUpload(String message) {
        Toast toast = Toast.makeText(this,message,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onSerieNotUpload(String error) {
        Toast toast = Toast.makeText(this,error,Toast.LENGTH_SHORT);
        toast.show();
    }
}
