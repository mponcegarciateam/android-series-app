package com.example.mario.ejercicio6.UI.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mario.ejercicio6.R;
import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.presenter.GetSeriePresenter;
import com.squareup.picasso.Picasso;

public class SeriesDescriptionActivity extends AppCompatActivity implements GetSeriePresenter.View {

    private ImageView imagen;
    private TextView titulo;
    private TextView descripcion;
    private GetSeriePresenter presenter;
    private static String SERIE_PRIMARY_KEY = "code";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series_description);

        imagen = (ImageView) findViewById(R.id.imageDescription);
        titulo = (TextView) findViewById(R.id.descriptionTitle);
        descripcion = (TextView) findViewById(R.id.description);
        presenter = new GetSeriePresenter();
        presenter.setView(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String code = bundle.getString(SERIE_PRIMARY_KEY);
            presenter.receiveData(SERIE_PRIMARY_KEY, code, this);
        }
    }
    public void loadValues (Serie serie){
        if(serie.getImage().equals("")){}
        else{
            Picasso.with(this).load(serie.getImage()).into(imagen);
        }
        titulo.setText(serie.getTitle());
        descripcion.setText(serie.getDescription());
    }

    @Override
    public void onLoadSuccess(Serie serie) {
        loadValues(serie);
    }

    @Override
    public void onLoadError(String error) {
        Toast toast = Toast.makeText(this,error,Toast.LENGTH_SHORT);
        toast.show();
    }
}

