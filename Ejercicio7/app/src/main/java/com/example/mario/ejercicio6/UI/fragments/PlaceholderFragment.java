package com.example.mario.ejercicio6.UI.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mario.ejercicio6.R;
import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.UI.SeriesAdapter;
import com.example.mario.ejercicio6.UI.activities.SeriesDescriptionActivity;
import com.example.mario.ejercicio6.presenter.GetSeriesPresenter;
import com.example.mario.ejercicio6.presenter.SynchronizePresenter;
import com.example.mario.ejercicio6.presenter.UpdateFavPresenter;
import com.example.mario.ejercicio6.presenter.UpdateMgPresenter;

import java.util.ArrayList;

/**
 * Created by Mario on 15/11/2016.
 */

public class PlaceholderFragment extends Fragment implements SeriesAdapter.ClickListener,GetSeriesPresenter.View, SynchronizePresenter.View, UpdateFavPresenter.View, UpdateMgPresenter.View{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String SERIES_PRIMARY_KEY = "code";
    private int fragmentNumber;
    private LinearLayoutManager lManager;
    private SeriesAdapter adapter;
    private RecyclerView recycler;
    private SynchronizePresenter syncPresenter;
    private GetSeriesPresenter getSeriesPresenter;
    private UpdateFavPresenter updateFavPresenter;
    private UpdateMgPresenter updateMgPresenter;
    private ImageButton ibmg;
    private TextView textView;

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int sectionNumber) {

        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            LoadGetSeriesPresenter();
            getSeriesPresenter.receiveData(getContext(), fragmentNumber);
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        LoadSynchronizePresenter();
        syncPresenter.receiveData(getContext());

        recycler = (RecyclerView) rootView.findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);
        lManager = new LinearLayoutManager(getContext());

        LoadGetSeriesPresenter();
        getSeriesPresenter.receiveData(getContext(), fragmentNumber);

        fragmentNumber = getArguments().getInt(ARG_SECTION_NUMBER);

        return rootView;
    }

    // This method create and set a view for a SynchronizePresenter class
    public void LoadSynchronizePresenter(){
        syncPresenter = new SynchronizePresenter();
        syncPresenter.setView(this);
    }

    // This method create and set a view for a GetSeriesPresenter class
    public void LoadGetSeriesPresenter(){
        getSeriesPresenter = new GetSeriesPresenter();
        getSeriesPresenter.setView(this);
    }

    // This method create and set a view for a UpdateFavPresenter class
    public void LoadUpdateFavPresenter(){
        updateFavPresenter = new UpdateFavPresenter();
        updateFavPresenter.setView(this);
    }

    // This method create and set a view for a UpdateMgPresenter class
    public void LoadUpdateMgPresenter(){
        updateMgPresenter = new UpdateMgPresenter();
        updateMgPresenter.setView(this);
    }

    @Override
    public void onItemClicked(Serie item, int position) {
        Intent intent = new Intent(getContext(), SeriesDescriptionActivity.class);
        intent.putExtra(SERIES_PRIMARY_KEY, item.getCode());
        startActivity(intent);
    }

    @Override
    public void onFavClicked(Serie item) {
        LoadUpdateFavPresenter();
        updateFavPresenter.receiveData(item, getContext());
    }

    @Override
    public void onMgClicked(Serie item, int position, ImageButton ibmg, TextView textView) {
        this.ibmg = ibmg;
        this.textView = textView;
        LoadUpdateMgPresenter();
        updateMgPresenter.receiveData(item, getContext(), position);
    }

    @Override
    public void onSyncSuccess() {
    }

    @Override
    public void onSyncError(String error) {
        Toast toast = Toast.makeText(getContext(),error,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onLoadSuccess(ArrayList<Serie> series) {
        if (series != null) {
            recycler.setLayoutManager(lManager);
            adapter = new SeriesAdapter(series, getContext(), this);
            recycler.setAdapter(adapter);
        }
        else {
            recycler.setLayoutManager(lManager);
            adapter = new SeriesAdapter(new ArrayList<Serie>() , getContext(), this);
            recycler.setAdapter(adapter);
        }
    }

    @Override
    public void onLoadError(String error) {
        Toast toast = Toast.makeText(getContext(),error,Toast.LENGTH_SHORT);
        toast.show();
    }

    // UpdateFavPresenter.View interface methods
    @Override
    public void onFavSuccess(String message) {
        Toast toast = Toast.makeText(getContext(),message,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onFavError(String error) {
        Toast toast = Toast.makeText(getContext(),error,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onMgSuccess(int position, boolean mg, int contmg) {
        textView.setText(String.valueOf(contmg));
        if (mg){
            ibmg.setImageResource(R.drawable.ic_thumb_up_blue_32dp);
        }
        else{
            ibmg.setImageResource(R.drawable.ic_thumb_up_black_32dp);
        }
    }

    @Override
    public void onMgError(String error) {
        Toast toast = Toast.makeText(getContext(),error,Toast.LENGTH_SHORT);
        toast.show();
    }

}