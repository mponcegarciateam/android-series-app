package com.example.mario.ejercicio6.interactor;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.Repository;

import java.util.ArrayList;
import java.util.concurrent.Executor;

/**
 * Created by Mario on 19/10/2016.
 */

public class GetFavSeriesInteractor implements GetSeriesInteractorInterface, Runnable {

    private MainThread mainThread;
    private Executor executor;
    private Callback callback;
    private Context context;
    private String SERIES_ERROR_MESSAGE = "Ha ocurrido un error";
    private String SERIES_FAV_KEY = "fav";

    public GetFavSeriesInteractor(Executor executor, MainThread mainThread, Context context){
        this.mainThread = mainThread;
        this.executor = executor;
        this.context = context;
    }

    public void execute(Callback callback) {
        this.callback = callback;
        //call method override "run" from interface runnable
        this.executor.execute(this);
    }

    @Override
    public void run() {
        try {
            Repository repository = new Repository(context);
            ArrayList<Serie> series = new ArrayList<>();
            for(int i = 0; i < repository.sizeSeries();i++){
                series.add(repository.queryGetSerie(SERIES_FAV_KEY,true));
            }
            notifySeriesLoaded(series);

            } catch (Throwable error) {
            notifyError();
        }
    }
    public void notifySeriesLoaded(final ArrayList<Serie> series){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(series);
            }
        });
    }
    public void notifyError(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(SERIES_ERROR_MESSAGE);
            }
        });
    }
}
