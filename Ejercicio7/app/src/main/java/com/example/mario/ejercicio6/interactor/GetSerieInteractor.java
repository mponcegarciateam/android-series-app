package com.example.mario.ejercicio6.interactor;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.GetSeriesInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.Repository;

import java.util.ArrayList;
import java.util.concurrent.Executor;

/**
 * Created by Mario on 19/10/2016.
 */

public class GetSerieInteractor implements GetSerieInteractorInterface, Runnable {

    private MainThread mainThread;
    private Executor executor;
    private GetSerieInteractorInterface.Callback callback;
    private String key;
    private String value;
    private Context context;
    private String SERIES_ERROR_MESSAGE = "Ha ocurrido un error";

    public GetSerieInteractor(Executor executor, MainThread mainThread, String key, String value, Context context){
        this.mainThread = mainThread;
        this.executor = executor;
        this.key = key;
        this.value = value;
        this.context = context;
    }

    public void execute(Callback callback) {
        this.callback = callback;
        //call method override "run" from interface runnable
        this.executor.execute(this);
    }

    @Override
    public void run() {
        try {
            Repository repository = new Repository(context);
            notifySeriesLoaded(repository.queryGetSerie(key,value));

            } catch (Throwable error) {
            notifyError();
        }
    }
    public void notifySeriesLoaded(final Serie serie){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(serie);
            }
        });
    }
    public void notifyError(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(SERIES_ERROR_MESSAGE);
            }
        });
    }
}
