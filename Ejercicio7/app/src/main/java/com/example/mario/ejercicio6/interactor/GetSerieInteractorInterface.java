package com.example.mario.ejercicio6.interactor;

import com.example.mario.ejercicio6.UI.Serie;

import java.util.ArrayList;

/**
 * Created by Mario on 19/10/2016.
 */

public interface GetSerieInteractorInterface {

    interface Callback {
        void onSuccess(Serie series);
        void onError(String error);
    }
}
