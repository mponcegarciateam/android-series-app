package com.example.mario.ejercicio6.interactor;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Mario on 19/10/2016.
 */

public interface GetSeriesInteractorInterface {

    interface Callback {
        void onSuccess(ArrayList<Serie> series);
        void onError(String error);
    }
}
