package com.example.mario.ejercicio6.interactor;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.MyFirebase;
import com.example.mario.ejercicio6.model.Repository;
import com.example.mario.ejercicio6.model.SerieDAO;
import com.google.firebase.database.DataSnapshot;

import java.util.concurrent.Executor;

/**
 * Created by Mario on 20/10/2016.
 */

public class SynchronizeInteractor implements Runnable, SynchronizeInteractorInterface{

    private MainThread mainThread;
    private Executor executor;
    private SynchronizeInteractorInterface.Callback callback;
    private Context context;
    private String SERIES_ERROR_MESSAGE = "No se han podido mostrar las series.";

    public SynchronizeInteractor(Executor executor, MainThread mainThread, Context context){
        this.mainThread = mainThread;
        this.executor = executor;
        this.context = context;
    }

    public void execute(SynchronizeInteractorInterface.Callback callback) {
        this.callback = callback;
        //call method override "run" from interface runnable
        this.executor.execute(this);
    }

    @Override
    public void run() {
        try {
            MyFirebase.loadData(new getDatasnapchot() {
                @Override
                public void onSuccess(DataSnapshot dataSnapshot) {
                    Repository repository = new Repository(context);
                    for (DataSnapshot entitySnapshot : dataSnapshot.getChildren()) {
                        SerieDAO serie = entitySnapshot.getValue(SerieDAO.class);
                        Serie serieFAV = repository.queryGetSerie("code",serie.getCode());
                        if (serieFAV != null && serieFAV.getUpdatedAt() < serie.getUpdatedAt()){
                            serie.setFav(serieFAV.getFav());
                            serie.setMg(serieFAV.getMg());
                            repository.save(serie);
                        }
                        else if(serieFAV == null) {
                            repository.save(serie);
                        }
                    }
                    notifySynchronizeSuccess();
                }

                @Override
                public void onError() {
                    notifySynchronizeError();
                }

            });
        }
        catch(Exception e){
            notifySynchronizeError();
        }
    }
    public void notifySynchronizeSuccess(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess();
            }
        });
    }

    public void notifySynchronizeError(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(SERIES_ERROR_MESSAGE);
            }
        });
    }
}
