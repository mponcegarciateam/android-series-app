package com.example.mario.ejercicio6.interactor;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by Mario on 20/10/2016.
 */

public interface SynchronizeInteractorInterface {

    interface Callback {
        void onSuccess();
        void onError(String error);
    }
    interface getDatasnapchot{
        void onSuccess(DataSnapshot dataSnapshot);
        void onError();
    }

}
