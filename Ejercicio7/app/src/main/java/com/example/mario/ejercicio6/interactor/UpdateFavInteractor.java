package com.example.mario.ejercicio6.interactor;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.Repository;
import com.example.mario.ejercicio6.model.SerieDAO;

import java.util.concurrent.Executor;

/**
 * Created by Mario on 02/11/2016.
 */

public class UpdateFavInteractor implements Runnable{

    private Serie serie;
    private MainThread mainThread;
    private Executor executor;
    private UpdateFavInteractorInterface.Callback callback;
    private Context context;
    private String UPDATE_SERIE_FAVTRUE = "Serie añadida a favoritos";
    private String UPDATE_SERIE_FAVFALSE = "Serie quitada de favoritos";
    private String UPDTE_SERIE_ERROR = "La serie no ha podido añadirse a favoritos debido a un error";

    public UpdateFavInteractor(MainThread mainThread, Executor executor, Serie serie, Context context) {
        this.serie = serie;
        this.mainThread = mainThread;
        this.executor = executor;
        this.context = context;
    }
    public void execute(UpdateFavInteractorInterface.Callback callback){
        this.callback = callback;
        executor.execute(this);
    }

    @Override
    public void run() {
        try {
            Repository repository = new Repository(context);
            SerieDAO dao = new SerieDAO();
            dao.setSerieDAO(serie);
            repository.save(dao);
            if (dao.getFav() == true)
                notifyUpdateSuccess(UPDATE_SERIE_FAVTRUE);
            else
                notifyUpdateSuccess(UPDATE_SERIE_FAVFALSE);
        }
        catch (Exception e){
            notifyUpdateError();
        }
    }
    public void notifyUpdateSuccess(final String message){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(message);
            }
        });
    }
    public void notifyUpdateError(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(UPDTE_SERIE_ERROR);
            }
        });
    }
}
