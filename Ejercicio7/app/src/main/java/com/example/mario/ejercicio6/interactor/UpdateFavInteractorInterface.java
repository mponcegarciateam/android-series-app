package com.example.mario.ejercicio6.interactor;

/**
 * Created by Mario on 02/11/2016.
 */

public interface UpdateFavInteractorInterface {
    interface Callback {
        void onSuccess(String message);
        void onError(String error);
    }
}
