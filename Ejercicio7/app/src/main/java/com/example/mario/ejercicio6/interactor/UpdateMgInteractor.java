package com.example.mario.ejercicio6.interactor;

import android.content.Context;
import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.MyFirebase;
import com.example.mario.ejercicio6.model.Repository;
import com.example.mario.ejercicio6.model.SerieDAO;
import com.google.firebase.database.DataSnapshot;

import java.util.concurrent.Executor;

/**
 * Created by Mario on 09/11/2016.
 */

public class UpdateMgInteractor implements Runnable{
    private Serie serie;
    private MainThread mainThread;
    private Executor executor;
    private UpdateMgInteractorInterface.Callback callback;
    private Context context;
    private int position;
    private String UPDTE_SERIE_ERROR = "Error";

    public UpdateMgInteractor(MainThread mainThread, Executor executor, Serie serie, Context context, int position) {
        this.serie = serie;
        this.mainThread = mainThread;
        this.executor = executor;
        this.context = context;
        this.position = position;
    }
    public void execute(UpdateMgInteractorInterface.Callback callback){
        this.callback = callback;
        executor.execute(this);
    }

    @Override
    public void run() {
        try {
            MyFirebase firebase = new MyFirebase();
            firebase.updateMg(serie.getCode(), serie.getMg(), new MyFirebase.CallbackUpdateMg() {
                @Override
                public void onSuccessUpdateMg(DataSnapshot dataSnapshot) {
                    Repository repository = new Repository(context);
                    SerieDAO dao = new SerieDAO();
                    dao.setSerieDAO(serie);
                    dao.setMeGusta_number(dataSnapshot.getValue(Long.class));
                    int contmg = dataSnapshot.getValue(Integer.class);
                    repository.save(dao);
                    notifyUpdateSuccess(position, dao.getMg(),contmg);
                }

                @Override
                public void onErrorUpdateMg(String error) {

                }
            });
        }
        catch (Exception e){
            notifyUpdateError();
        }
    }
    public void notifyUpdateSuccess(final int position, final boolean mg, final int contmg){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(position, mg, contmg);
            }
        });
    }
    public void notifyUpdateError(){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(UPDTE_SERIE_ERROR);
            }
        });
    }
}
