package com.example.mario.ejercicio6.interactor;

/**
 * Created by Mario on 09/11/2016.
 */

public interface UpdateMgInteractorInterface {
    interface Callback {
        void onSuccess(int position, boolean value, int contmg);
        void onError(String error);
    }
}
