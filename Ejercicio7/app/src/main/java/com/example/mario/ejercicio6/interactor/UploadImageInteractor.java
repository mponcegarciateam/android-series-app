package com.example.mario.ejercicio6.interactor;

import android.content.Context;
import android.widget.ImageView;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.MyFirebase;

import java.util.concurrent.Executor;

/**
 * Created by Mario on 27/10/2016.
 */

public class UploadImageInteractor implements Runnable{

    private UploadImageInteractorInterface.Callback callback;
    private Executor executor;
    private MainThread mainThread;
    private String title;
    private ImageView imageView;

    public UploadImageInteractor(Executor executor, MainThread mainThread, ImageView imageView, String title){

        this.executor = executor;
        this.mainThread = mainThread;
        this.title = title;
        this.imageView = imageView;

    }

    public void execute(UploadImageInteractorInterface.Callback callback){
        this.callback = callback;
        executor.execute(this);
    }

    @Override
    public void run() {
        MyFirebase firebase = new MyFirebase();
        firebase.uploadImage(imageView, title, new MyFirebase.CallbackUploadImage() {
            @Override
            public void onSuccessUploadImage(String urlImage) {
                notifyImageUpload(urlImage);
            }

            @Override
            public void onErrorUploadImage(String error) {
                notifyImageNotUpload(error);
            }
        });
    }

    public void notifyImageUpload(final String urlImage){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(urlImage);
            }
        });
    }

    public void notifyImageNotUpload(final String error){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(error);
            }
        });
    }
}
