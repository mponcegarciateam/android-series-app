package com.example.mario.ejercicio6.interactor;

/**
 * Created by Mario on 27/10/2016.
 */

public interface UploadImageInteractorInterface {

    interface Callback{
        void onSuccess(String url);
        void onError(String error);
    }
}
