package com.example.mario.ejercicio6.interactor;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.model.MyFirebase;

import java.util.concurrent.Executor;

/**
 * Created by Mario on 27/10/2016.
 */

public class UploadSerieInteractor implements Runnable{

    private Executor executor;
    private MainThread mainThread;
    private Serie serie;
    private UploadSerieInteractorInterface.Callback callback;
    private String SERIES_ERROR_MESSAGE = "Ha ocurrido un error.";
    private String SERIES_SUCCESS_MESSAGE = "Serie subida con éxito.";

    public UploadSerieInteractor(Executor executor, MainThread mainThread, Serie serie){
        this.executor = executor;
        this.mainThread = mainThread;
        this.serie = serie;
    }

    public void execute(UploadSerieInteractorInterface.Callback callback){
        this.callback = callback;
        executor.execute(this);
    }

    @Override
    public void run() {
        try {
            MyFirebase.updateSerie(serie);
            notifySerieUpload(SERIES_SUCCESS_MESSAGE);
        }
        catch(Exception e){
            notifySerieNotUpload(SERIES_ERROR_MESSAGE);
        }
    }

    public void notifySerieUpload(final String message){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(message);
            }
        });
    }
    public void notifySerieNotUpload(final String error){
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(error);
            }
        });
    }
}
