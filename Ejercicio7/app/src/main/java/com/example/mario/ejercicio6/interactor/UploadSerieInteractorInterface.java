package com.example.mario.ejercicio6.interactor;


/**
 * Created by Mario on 27/10/2016.
 */

public interface UploadSerieInteractorInterface {
    interface Callback{
        void onSuccess(String message);
        void onError(String error);
    }
}
