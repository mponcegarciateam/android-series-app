package com.example.mario.ejercicio6.model;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.SynchronizeInteractorInterface;
import com.example.mario.ejercicio6.interactor.UploadImageInteractorInterface;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.ByteArrayOutputStream;

import static android.content.ContentValues.TAG;

/**
 * Created by Mario on 29/09/2016.
 */

public class MyFirebase {

    final static FirebaseDatabase rootRef = FirebaseDatabase.getInstance();
    final static DatabaseReference childRef = rootRef.getReference().child("SERIES");
    final static FirebaseStorage storage = FirebaseStorage.getInstance();
    final static StorageReference storageRef = storage.getReferenceFromUrl("gs://seriesdev-30b8b.appspot.com");
    final static String DIR_IMAGES_FIREBASE = "images/";
    static String downloadurl = null;

    public MyFirebase(){
    }

    public static String getNewId() {
        String id = childRef.push().getKey();
        return id;
    }
    public static void updateSerie(Serie serie) {
        childRef.child(serie.getCode()).updateChildren(serie.toMap());
    }
    public static void loadData( final SynchronizeInteractorInterface.getDatasnapchot callback) {
        childRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()) {
                    callback.onSuccess(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                callback.onError();
            }
        });
    }

    public void updateMg(final String code, final boolean mg, final CallbackUpdateMg callbackUpdateMg) {
        try {
            final DatabaseReference ref = childRef.child(code).child("meGusta_number");
            ref.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    long value = (long) mutableData.getValue();
                    if (mg){
                        value += 1;
                    }
                    else {
                        value -= 1;
                    }
                    mutableData.setValue(value);
                    childRef.child(code).child("updatedAt").setValue(System.currentTimeMillis());
                    return Transaction.success(mutableData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                    if (databaseError == null)
                        callbackUpdateMg.onSuccessUpdateMg(dataSnapshot);
                    else
                        callbackUpdateMg.onErrorUpdateMg(databaseError.toString());
                }
            });
        }
        catch (Exception e) {
            callbackUpdateMg.onErrorUpdateMg("Error");
        }
    }

    public void uploadImage(ImageView imageView, String title, final CallbackUploadImage callback){
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = storageRef.child(DIR_IMAGES_FIREBASE + title).putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                callback.onErrorUploadImage(exception.toString());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                if(taskSnapshot!=null) {
                    try {
                        downloadurl = taskSnapshot.getMetadata().getDownloadUrl().toString();
                        callback.onSuccessUploadImage(downloadurl);

                    }
                    catch(java.lang.NullPointerException error){
                    }
                }
            }
        });
    }
    public interface CallbackUploadImage {
        void onSuccessUploadImage(String urlImage);
        void onErrorUploadImage(String error);
    }
    public interface CallbackUpdateMg {
        void onSuccessUpdateMg(DataSnapshot dataSnapshot);
        void onErrorUpdateMg(String error);
    }
}
