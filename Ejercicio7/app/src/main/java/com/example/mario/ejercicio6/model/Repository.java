package com.example.mario.ejercicio6.model;

import android.content.Context;
import com.example.mario.ejercicio6.UI.Serie;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Repository {

    Realm realm;

    public Repository(Context context) {
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();
    }

    public void save(ArrayList<Serie> series) {
        realm.beginTransaction();
        for (int i = 0; i < series.size(); i++) {
            SerieDAO dao = new SerieDAO();
            dao.setSerieDAO(series.get(i));
            realm.copyToRealmOrUpdate(dao);
        }
        realm.commitTransaction();
    }

    public void save (SerieDAO serie){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(serie);
        realm.commitTransaction();
    }

    public Serie setSerie(SerieDAO dao) {
        Serie serie = new Serie();
        serie.setCode(dao.getCode());
        serie.setDescription(dao.getDescription());
        serie.setTitle(dao.getTitle());
        serie.setImage(dao.getImage());
        serie.setYear(dao.getYear());
        serie.setFav(dao.getFav());
        serie.setMg(dao.getMg());
        serie.setCreatedAt(dao.getCreatedAt());
        serie.setUpdatedAt(dao.getUpdatedAt());
        serie.setMeGusta_number(dao.getMeGusta_number());
        return serie;
    }

    public ArrayList<Serie> queryGetSeries(String query, String value) {
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).equalTo(query, value).findAll();
        if(r.size()==0)
            return null;
        else{
            ArrayList<Serie> series = new ArrayList<>();
            for(int i = 0; i < r.size();i++){
                RealmObject serie = r.get(i);
                SerieDAO dao = (SerieDAO) serie;
                series.add(setSerie(dao));
            }
            return series;
        }
    }

    public Serie queryGetSerie(String query, String value) {
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).equalTo(query, value).findAll();
        if(r.size()==0)
            return null;
        else{
            RealmObject serie = r.get(0);
            SerieDAO dao = (SerieDAO) serie;
            return setSerie(dao);
        }
    }

    public Serie queryGetSerie(String query, boolean value) {
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).equalTo(query, value).findAll();
        if(r.size()==0)
            return null;
        else{
            RealmObject serie = r.get(0);
            SerieDAO dao = (SerieDAO) serie;
            return setSerie(dao);
        }
    }

    public ArrayList<Serie> queryGetSeries(String query, boolean value) {
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).equalTo(query, value).findAll();
        if(r.size()==0)
            return null;
        else{
            ArrayList<Serie> series = new ArrayList<>();
            for(int i = 0; i < r.size();i++)
                series.add(setSerie(r.get(i)));
            return series;
        }
    }

    public Serie queryGetSeries(int value){
        RealmResults<SerieDAO> r = realm.where(SerieDAO.class).findAll();
        if(r.size()==0)
            return null;
        else{
            RealmObject serie = r.get(value);
            SerieDAO dao = (SerieDAO) serie;
            return setSerie(dao);
            }
    }
    public int sizeSeries(){
        return realm.where(SerieDAO.class).findAll().size();
    }
}
