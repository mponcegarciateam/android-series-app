package com.example.mario.ejercicio6.model;

import com.example.mario.ejercicio6.UI.Serie;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SerieDAO extends RealmObject {
    @PrimaryKey
    private String code;

    private String image;
    private String title;
    private String description;
    private String year;
    private boolean fav;
    private boolean mg;
    private long createdAt;
    private long updatedAt;
    private boolean active;
    private long meGusta_number;

    public void setSerieDAO(Serie serie) {

        code = serie.getCode();
        image = serie.getImage();
        title = serie.getTitle();
        description = serie.getDescription();
        year = serie.getYear();
        fav = serie.getFav();
        mg = serie.getMg();
        createdAt = serie.getCreatedAt();
        updatedAt = serie.getUpdatedAt();
        meGusta_number = serie.getMeGusta_number();

    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public void setMg(boolean mg) {
        this.mg = mg;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setMeGusta_number(long meGusta_number) {
        this.meGusta_number = meGusta_number;
    }

    public String getCode() {
        return code;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getYear() {
        return year;
    }

    public boolean getFav() {
        return fav;
    }

    public boolean getMg() {
        return mg;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public long getMeGusta_number() {
        return meGusta_number;
    }
}