package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.GetSerieInteractor;
import com.example.mario.ejercicio6.interactor.GetSerieInteractorInterface;
import com.example.mario.ejercicio6.interactor.GetSeriesInteractor;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 19/10/2016.
 */

public class GetSeriePresenter implements GetSeriePresenterInterface{

    private View view;
    private GetSerieInteractor interactor;

    public GetSeriePresenter(){

    }

    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    @Override
    public void receiveData(String key, String value, Context context) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new GetSerieInteractor(executor, mainThread, key, value, context);
        interactor.execute(new GetSerieInteractorInterface.Callback() {
            @Override
            public void onSuccess(Serie serie) {
                view.onLoadSuccess(serie);
            }

            @Override
            public void onError(String error) {
                view.onLoadError(error);
            }
        });
    }
    public interface View {
        void onLoadSuccess(Serie serie);
        void onLoadError(String error);
    }
}
