package com.example.mario.ejercicio6.presenter;

import android.content.Context;

/**
 * Created by Mario on 19/10/2016.
 */

public interface GetSeriePresenterInterface {
    public void receiveData(String key, String value, Context context);
}
