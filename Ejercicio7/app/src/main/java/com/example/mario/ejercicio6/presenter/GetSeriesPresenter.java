package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.GetSeriesInteractor;
import com.example.mario.ejercicio6.interactor.GetSeriesInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 19/10/2016.
 */

public class GetSeriesPresenter implements GetSeriesPresenterInterface {

    private View view;
    private GetSeriesInteractor interactor;

    public GetSeriesPresenter(){

    }
    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    @Override
    public void receiveData(Context context, int fragmentNumber) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new GetSeriesInteractor(executor, mainThread, context, fragmentNumber);
        interactor.execute(new GetSeriesInteractorInterface.Callback() {
            @Override
            public void onSuccess(ArrayList<Serie> series) {
                view.onLoadSuccess(series);
            }

            @Override
            public void onError(String error) {
                view.onLoadError(error);
            }
        });

    }

    public interface View {
        void onLoadSuccess(ArrayList<Serie> series);
        void onLoadError(String error);
    }
}
