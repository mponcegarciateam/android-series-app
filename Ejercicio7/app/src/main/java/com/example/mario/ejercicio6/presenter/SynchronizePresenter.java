package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.interactor.SynchronizeInteractor;
import com.example.mario.ejercicio6.interactor.SynchronizeInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 20/10/2016.
 */

public class SynchronizePresenter implements SynchronizePresenterInterface{

    private View view;
    private SynchronizeInteractor interactor;

    public SynchronizePresenter(){

    }

    public void setView(SynchronizePresenter.View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    @Override
    public void receiveData(Context context) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new SynchronizeInteractor(executor, mainThread, context);
        interactor.execute(new SynchronizeInteractorInterface.Callback() {
            @Override
            public void onSuccess() {
                view.onSyncSuccess();
            }

            @Override
            public void onError(String error) {
                view.onSyncError(error);
            }
        });
    }


    public interface View {
        void onSyncSuccess();
        void onSyncError(String error);
    }

}
