package com.example.mario.ejercicio6.presenter;

import android.content.Context;

/**
 * Created by Mario on 20/10/2016.
 */

public interface SynchronizePresenterInterface {
    void receiveData(Context context);
}
