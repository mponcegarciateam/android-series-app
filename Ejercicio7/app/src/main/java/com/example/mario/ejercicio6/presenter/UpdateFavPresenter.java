package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.UpdateFavInteractor;
import com.example.mario.ejercicio6.interactor.UpdateFavInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 02/11/2016.
 */

public class UpdateFavPresenter implements UpdateFavPresenterInterface {

    private UpdateFavInteractor interactor;
    private View view;

    public UpdateFavPresenter() {

    }

    public void setView(UpdateFavPresenter.View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }


    @Override
    public void receiveData(Serie serie ,Context context) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new UpdateFavInteractor(mainThread,executor,serie, context);
        interactor.execute(new UpdateFavInteractorInterface.Callback() {
            @Override
            public void onSuccess(String message) {
                view.onFavSuccess(message);
            }

            @Override
            public void onError(String error) {
                view.onFavError(error);
            }
        });
    }

    public interface View {
        void onFavSuccess(String message);
        void onFavError(String error);
    }
}
