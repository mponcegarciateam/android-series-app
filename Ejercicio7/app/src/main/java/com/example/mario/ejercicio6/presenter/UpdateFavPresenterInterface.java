package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;

/**
 * Created by Mario on 02/11/2016.
 */

public interface UpdateFavPresenterInterface {
    void receiveData(Serie serie,Context context);
}
