package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.UpdateMgInteractor;
import com.example.mario.ejercicio6.interactor.UpdateFavInteractorInterface;
import com.example.mario.ejercicio6.interactor.UpdateMgInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 08/11/2016.
 */

public class UpdateMgPresenter implements UpdateMgPresenterInterface {

    private UpdateMgInteractor interactor;
    private View view;

    public UpdateMgPresenter () {

    }

    public void setView(UpdateMgPresenter.View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }


    @Override
    public void receiveData(Serie serie, Context context, int position) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new UpdateMgInteractor(mainThread,executor,serie,context, position);
        interactor.execute(new UpdateMgInteractorInterface.Callback() {
            @Override
            public void onSuccess(int position, boolean mg, int contmg) {
                view.onMgSuccess(position, mg, contmg);
            }

            @Override
            public void onError(String error) {
                view.onMgError(error);
            }
        });
    }

    public interface View {
        void onMgSuccess(int position, boolean mg, int contmg);
        void onMgError(String error);
    }
}
