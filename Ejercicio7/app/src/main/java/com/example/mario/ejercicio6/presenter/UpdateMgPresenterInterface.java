package com.example.mario.ejercicio6.presenter;

import android.content.Context;

import com.example.mario.ejercicio6.UI.Serie;

/**
 * Created by Mario on 08/11/2016.
 */

public interface UpdateMgPresenterInterface {
    void receiveData(Serie serie,Context context, int position);
}
