package com.example.mario.ejercicio6.presenter;

import android.content.Context;
import android.widget.ImageView;
import com.example.mario.ejercicio6.interactor.UploadImageInteractor;
import com.example.mario.ejercicio6.interactor.UploadImageInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 27/10/2016.
 */

public class UploadImagePresenter implements UploadImagePresenterInterface{

    private View view;
    private UploadImageInteractor interactor;

    public UploadImagePresenter(){

    }

    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    @Override
    public void ReceiveData(ImageView imageView, String title) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new UploadImageInteractor(executor,mainThread,imageView,title);
        interactor.execute(new UploadImageInteractorInterface.Callback() {
            @Override
            public void onSuccess(String url) {
                view.onImageUpload(url);
            }

            @Override
            public void onError(String error) {
                view.onImageNotUpload(error);
            }
        });

    }


    public interface View {
        void onImageUpload(String url);
        void onImageNotUpload(String error);
    }
}
