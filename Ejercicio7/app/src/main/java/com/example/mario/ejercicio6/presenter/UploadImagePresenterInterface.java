package com.example.mario.ejercicio6.presenter;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by Mario on 27/10/2016.
 */

public interface UploadImagePresenterInterface {
    void ReceiveData(ImageView imageView, String title);
}
