package com.example.mario.ejercicio6.presenter;

import com.example.mario.ejercicio6.UI.Serie;
import com.example.mario.ejercicio6.interactor.UploadSerieInteractor;
import com.example.mario.ejercicio6.interactor.UploadSerieInteractorInterface;
import com.example.mario.ejercicio6.interactor.executor.MainThread;
import com.example.mario.ejercicio6.interactor.executor.MainThreadImp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mario on 27/10/2016.
 */

public class UploadSeriePresenter implements UploadSeriePresenterInterface {

    private View view;
    private UploadSerieInteractor interactor;

    public UploadSeriePresenter(){

    }

    public void setView(View view) {
        if (view == null) {
            throw new IllegalArgumentException("You can't set a null view");
        }
        this.view = view;
    }

    @Override
    public void ReceiveData(Serie serie) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        MainThread mainThread = new MainThreadImp();
        interactor = new UploadSerieInteractor(executor,mainThread,serie);
        interactor.execute(new UploadSerieInteractorInterface.Callback() {
            @Override
            public void onSuccess(String message) {
                view.onSerieUpload(message);
            }

            @Override
            public void onError(String error) {
                view.onSerieNotUpload(error);
            }
        });
    }
    public interface View {
        void onSerieUpload(String message);
        void onSerieNotUpload(String error);
    }
}
