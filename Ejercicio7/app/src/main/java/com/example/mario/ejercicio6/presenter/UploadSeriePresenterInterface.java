package com.example.mario.ejercicio6.presenter;

import com.example.mario.ejercicio6.UI.Serie;

/**
 * Created by Mario on 27/10/2016.
 */

public interface UploadSeriePresenterInterface {
    void ReceiveData(Serie serie);
}
